// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDn-YLo7nHI_000k7PARvaciWVJE1fCuNE",
    authDomain: "congresovaloragregado.firebaseapp.com",
    databaseURL: "https://congresovaloragregado.firebaseio.com",
    projectId: "congresovaloragregado",
    storageBucket: "congresovaloragregado.appspot.com",
    messagingSenderId: "5935429860"
    /*

        authDomain: "congresovaloragregado.firebaseapp.com",

    apiKey: "AIzaSyDNN4yxqEvqGjl7NEjpUjPxxIX2vFAioWM",
    authDomain: "congresovalorcompartido-27ae6.firebaseapp.com",
    databaseURL: "https://congresovalorcompartido-27ae6.firebaseio.com",
    projectId: "congresovalorcompartido-27ae6",
    storageBucket: "congresovalorcompartido-27ae6.appspot.com",
    messagingSenderId: "798834878424"
    */
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
