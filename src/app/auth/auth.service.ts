import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { User } from './user';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore} from '@angular/fire/firestore';
import {MatSnackBar} from '@angular/material';
import { UserService } from '../services/user.service';
import { Observable } from 'rxjs';
import { AppServiceService } from '../services/app-service.service';
import { Usuario } from '../models/usuario';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedIn = new BehaviorSubject<boolean>(false); // {1}
  private newUserInfo: any;
  public email: String;
  public usuario: Observable<{}[]>;
  constructor(
    private appService: AppServiceService,
    private userService: UserService,
    private router: Router,
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    private snackBar: MatSnackBar,
  ) {
    this.afAuth.auth.onAuthStateChanged( (user) => {
      if ( user ) {
        this.loggedIn.next( true );
      } else {
        this.loggedIn.next( false );
      }
    });
  }

  get isLoggedIn() {
    return this.loggedIn.asObservable(); // {2}
  }

  getEmail() {
    return this.email;
  }

  login(user: User) {
    const that = this;
    this.afs.doc(`usuarios/${user.userName}`).valueChanges().subscribe(variable => {

      if ( variable !== undefined ) {

        that.newUserInfo = variable;
        this.afAuth
            .auth
            .signInWithEmailAndPassword( variable['correo'], user.password )
            .then(function(value) {
              const userInfo: Usuario = {
                correo: variable['correo'],
                grupo: variable['grupo'],
                laboratorio: variable['laboratorio'],
                nombre: variable['nombre'],
                puntos_individuales: variable['puntos_individuales'],
                puntos_disponibles: variable['puntos_disponibles'],
                puntos_invertidos: variable['puntos_invertidos'],
                inversiones: variable['inversiones'],
                puntos_totales: variable['puntos_totales'],
                tipo_usuario: variable['tipo_usuario'],
                idCongreso: variable['idCongreso']
              };
              that.email = user.userName;
              that.loggedIn.next(true);
              that.userService.userName = user.userName.toString();
              that.userService.userName = user.userName.toString();
              that.userService.setInformationUser( userInfo );
              if ( variable['tipo_usuario'] === '1' ) {
                that.router.navigate(['/admin']);
              } else if ( variable['tipo_usuario'] === '2') {
                that.router.navigate(['/coor']);
              } else if ( variable['tipo_usuario'] === '3' ) {
                that.router.navigate(['/']);
              }
            })
            .catch( error => {
              console.log( error );
              let message = 'Error al comprobar datos de acceso';

              if ( error.code === 'auth/invalid-email' ) {
                message = 'Direccion de correo invalida';
              } else if ( error.code === 'auth/user-disabled' ) {
                message = 'Usuario deshabilitado';
              } else if ( error.code === 'auth/user-not-found' ) {
                message = 'Usuario no registrado';
              } else if ( error.code === 'auth/wrong-password' ) {
                message = 'Contraseña incorrecta';
              }

              this.snackBar.open(message, 'Aceptar', {
                duration: 1500,
              });
            });
      } else {
        this.snackBar.open('Usuario no registrado, por favor verifique', 'Aceptar', {
          duration: 1500,
        });
      }
    });
  }

  logout() {                            // {4}
    this.afAuth.auth.signOut();
    this.loggedIn.next(false);
    this.router.navigate(['/login']);
  }
}

