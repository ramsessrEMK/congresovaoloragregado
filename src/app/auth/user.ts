export interface Roles {
    admin?: boolean;
    coordinador?: boolean;
    usuarioFinal?: boolean;
}
export interface User {
    userName: string;
    email: string;
    password: string;
    tipoUsuario: String;
    grupo: String;
    laboratorio: String;
    puntosIndividuales: number;
    puntosTotales: number;
}

