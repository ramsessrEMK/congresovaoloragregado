export interface Usuario {
    correo: string;
    grupo: string;
    laboratorio: string;
    nombre: string;
    puntos_individuales: number;
    puntos_disponibles: number;
    puntos_invertidos: Object[];
    inversiones: Object[];
    puntos_totales: number;
    tipo_usuario: string;
    idCongreso: number;
}
