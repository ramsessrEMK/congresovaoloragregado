import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserService } from '../services/user.service';
import { AuthService } from '../auth/auth.service';
import { AppServiceService } from '../services/app-service.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css'],
})
export class NavBarComponent {

  private tipoUsuario: string;
  private nombreCongreso: string;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private userService: UserService,
    private appService: AppServiceService,
    private authService: AuthService
  ) {
    this.tipoUsuario = userService.getInformationUser().tipo_usuario;
    this.nombreCongreso = appService.getInformacionCongreso( userService.getIdCongreso() )['nombre_congreso'];
  }

  cerrarSesion() {
    this.authService.logout();
  }

}
