import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AppRoutingModule } from './/app-routing.module';
import { LogInComponent } from './components/log-in/log-in.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ScheduleComponent } from './components/schedule/schedule.component';
import { InvestmentsComponent } from './components/investments/investments.component';
import { DesignThinkingComponent } from './components/design-thinking/design-thinking.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatFormField,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatCardModule,
  MatDialogModule,
  MatTableModule,
  MatProgressSpinnerModule,
  MatSnackBarModule,
  MatExpansionPanel,
  MatExpansionModule,
  MatTabsModule,
  MatGridListModule,
  MatPaginatorModule,
  MatCheckboxModule
} from '@angular/material';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { environment } from '../environments/environment';
import { HomeLayoutComponent } from './layouts/home-layout/home-layout.component';
import { LoginLayoutComponent } from './layouts/login-layout/login-layout.component';
import {AdminLayoutComponent} from './layouts/admin-layout/admin-layout.component';
import {CoordinadoresLayoutComponent} from './layouts/coordinadores-layout/coordinadores-layout.component';
import { ChartsModule } from 'ng2-charts';
import { AuthGuard } from './auth/auth.guard';
import { HomeCoordinadoresComponent } from './components/home-coordinadores/home-coordinadores.component';
import { HomeAdminComponent } from './components/home-admin/home-admin.component';
import { CrearOpcionesInversionComponent } from './components/crear-opciones-inversion/crear-opciones-inversion.component';
import { CrearUsuarioComponent } from './components/crear-usuario/crear-usuario.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LaboratoriosComponent, DialogOverviewExampleDialogComponent } from './components/laboratorios/laboratorios.component';
import { AdminRondasInversionComponent } from './components/admin-rondas-inversion/admin-rondas-inversion.component';
import { NuevoCriterioInversionComponent } from './components/nuevo-criterio-inversion/nuevo-criterio-inversion.component';
import { CalificarIdeaModalComponent } from './components/calificar-idea-modal/calificar-idea-modal.component';
import { NgxPaginationModule } from 'ngx-pagination';
import {
  DialogoCrearNuevaRondaInversionComponent
} from './components/dialogo-crear-nueva-ronda-inversion/dialogo-crear-nueva-ronda-inversion.component';
import { AdministrarAgendaComponent } from './components/administrar-agenda/administrar-agenda.component';
import { DialogAgregarEventoComponent } from './components/dialog-agregar-evento/dialog-agregar-evento.component';
import { CrearCongresoComponent } from './components/crear-congreso/crear-congreso.component';
import { CommonModule } from '@angular/common';
import { DialogCrearNuevoUsuarioComponent } from './components/dialog-crear-nuevo-usuario/dialog-crear-nuevo-usuario.component';

@NgModule({
  declarations: [
    AppComponent,
    LogInComponent,
    SignUpComponent,
    ProfileComponent,
    ScheduleComponent,
    InvestmentsComponent,
    DesignThinkingComponent,
    NavBarComponent,
    HomeLayoutComponent,
    LoginLayoutComponent,
    AdminLayoutComponent,
    CoordinadoresLayoutComponent,
    HomeCoordinadoresComponent,
    HomeAdminComponent,
    CrearOpcionesInversionComponent,
    CrearUsuarioComponent,
    LaboratoriosComponent,
    DialogOverviewExampleDialogComponent,
    CalificarIdeaModalComponent,
    AdminRondasInversionComponent,
    NuevoCriterioInversionComponent,
    DialogoCrearNuevaRondaInversionComponent,
    AdministrarAgendaComponent,
    DialogAgregarEventoComponent,
    CrearCongresoComponent,
    DialogCrearNuevoUsuarioComponent,
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule, // imports firebase/storage only needed for storage features
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatSelectModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatDialogModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatExpansionModule,
    ChartsModule,
    MatTabsModule,
    NgbModule,
    MatGridListModule,
    MatPaginatorModule,
    NgxPaginationModule,
    CommonModule,
    MatCheckboxModule
  ],
  entryComponents: [
    DialogOverviewExampleDialogComponent,
    CalificarIdeaModalComponent,
    NuevoCriterioInversionComponent,
    DialogoCrearNuevaRondaInversionComponent,
    CrearCongresoComponent,
    DialogAgregarEventoComponent,
    DialogCrearNuevoUsuarioComponent
  ],
  providers: [
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
