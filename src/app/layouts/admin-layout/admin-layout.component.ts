import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { HomeAdminComponent } from 'src/app/components/home-admin/home-admin.component';

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styles: []
})
export class AdminLayoutComponent implements OnInit {

  private homeAdminComponent: HomeAdminComponent;
  constructor(
    private authService: AuthService,
  ) { }

  ngOnInit() {
  }

  cerrarSesion() {
    this.authService.logout();
  }

}
