import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { UserService } from '../../services/user.service';
import { MatSnackBar } from '@angular/material';
import { AppServiceService } from 'src/app/services/app-service.service';

/*
export interface Event {
  name: string;
  startTime: string;
  finalHour: string;
}

const SCHEDULE: Event[] = [
  {startTime: '7:30 am', name: 'Registro', finalHour: '8:00 am'},
  {startTime: '8:00 am', name: 'Instalación. Palabras de Jorge Mario Diaz', finalHour: '8:15 am'},
  {startTime: '8:15 am', name: 'Intervención, ¿Cuales son los nuevos lideres que requiere el país?', finalHour: '8:45 am'},
  {
    startTime: '8:45 am',
    name: 'Conversatorio. Valor compartido una oportunidad para desarrollar más y mejores negocios',
    finalHour: '9:30 am'
  },
  {startTime: '9:30 am', name: 'Conferencia. Financión para negocios de valor compartido y Objetivos de desarrollo', finalHour: '10:15 am'},
  {startTime: '10:15 am', name: 'Receso', finalHour: '10:30 am'},
  {startTime: '10:30', name: 'Laboratorios de ideación', finalHour: '1:30'},
  {startTime: '', name: '1. Valor compartido en el sector servicios', finalHour: ''},
  {startTime: '', name: '2. Valor compartido y movilidad sostenible', finalHour: ''},
  {startTime: '', name: '3. Valor compartido y problemáticas sociales y ambientales.', finalHour: ''},
  {startTime: '', name: '4. Taller creativo. ¿Cómo pensar de manera creativa usando la recursidad como principio?', finalHour: ''},
  {startTime: '', name: '(almuerzo incluido)', finalHour: ''},
  {
    startTime: '1:30 pm',
    name: 'Presentación Pitches. Proyectos convocados a ganar el premio en la categoría Emprendimientos.',
    finalHour: '2:30 pm'
  },
  {
    startTime: '2:30 pm',
    name: 'Presentación ideas finalistas de los laboratorios. Votación del público para elección del ganador',
    finalHour: '3:00 pm'
  },
  {startTime: '2:45 pm', name: 'Prmiación de ideas Laboratorios y Pitches', finalHour: '3:00 pm'},
];
*/

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit {

  private displayedColumns: string[] = ['horario', 'descripcion'];
  private calendario: any[];
  private userInformation: any;
  constructor(
    private userService: UserService,
    private appService: AppServiceService,
    private afs: AngularFirestore,
    private snackBar: MatSnackBar
  ) {
    this.userInformation = userService.getInformationUser();
    this.calendario = appService.getCalendario( this.userInformation.idCongreso )[0]['calendario'];
  }

  ngOnInit() {}
}
