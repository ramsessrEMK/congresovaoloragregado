import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { AuthService } from '../../auth/auth.service';
import {
  Router
} from '@angular/router';
import { User } from 'src/app/auth/user';
import { AppServiceService } from 'src/app/services/app-service.service';
import { Usuario } from 'src/app/models/usuario';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  private doughnutChartLabels: string[] = ['Puntaje individual', 'Puntaje total'];
  private doughnutChartData: number[];
  private doughnutChartType = 'doughnut';
  private email: String;
  private grupo: String;
  private laboratorio: String;
  private puntajeTotal: Number;
  private puntajeIndividual: Number;
  private usuario: Usuario;
  private appInfo: any;
  constructor(
    private userService: UserService,
    private authService: AuthService,
    private router: Router,
    private appService: AppServiceService
  ) {
    this.usuario = userService.getInformationUser();
    this.appInfo = appService.getInformacionCongreso( this.usuario.idCongreso )['ronda_inversion_actual'];
    if (  this.usuario.tipo_usuario === '1'  ) {
      this.router.navigate(['/admin']);
    } else if (  this.usuario.tipo_usuario === '2'  ) {
      this.router.navigate(['/coor']);
    }
    if ( this.usuario.puntos_individuales === 0 && this.appInfo > 1 ) {
      userService.actualizarPuntajeUsuario( 1000, 1000 );
    }
  }

  ngOnInit() {
    this.email = this.usuario.correo;
    this.grupo = ((this.usuario.grupo === '') ? 'vacio' : this.usuario.grupo);
    this.laboratorio = ((this.usuario.laboratorio === '') ? 'vacio' : this.usuario.laboratorio);
    this.doughnutChartData = [
      this.usuario.puntos_individuales,
      this.usuario.puntos_totales
    ];
    this.puntajeIndividual = this.usuario.puntos_individuales;
    this.puntajeTotal = this.usuario.puntos_totales;
  }

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }
}
