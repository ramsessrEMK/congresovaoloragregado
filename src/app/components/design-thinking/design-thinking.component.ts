import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatSnackBar } from '@angular/material';
import { UserService } from 'src/app/services/user.service';
import { AppServiceService } from 'src/app/services/app-service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-design-thinking',
  templateUrl: './design-thinking.component.html',
  styleUrls: ['./design-thinking.component.css']
})
export class DesignThinkingComponent implements OnInit {

  form: FormGroup;                    // {1}
  private formSubmitAttempt: boolean; // {2}
  private enaviarIdea = false;
  private appInfo: any;
  private idCongreso: any;
  constructor(
    private fb: FormBuilder,
    private afs: AngularFirestore,
    private snackBar: MatSnackBar,
    private userService: UserService,
    private appInformation: AppServiceService,
    private route: ActivatedRoute
  ) {
    appInformation.getAppInfo()
    .subscribe( value => {
      this.appInfo = value;
    });
  }

  ngOnInit() {
    this.form = this.fb.group({     // {5}
      nameIdea: ['', Validators.required],
      opportunity: ['', Validators.required],
      description: ['', Validators.required],
      impact: ['', Validators.required],
    });
  }

  isFieldInvalid(field: string) { // {6}
    return (
      (!this.form.get(field).valid && this.form.get(field).touched) ||
      (this.form.get(field).untouched && this.formSubmitAttempt)
    );
  }


  onSubmit() {
    if (this.form.valid) {
      this.idCongreso = this.userService.getIdCongreso();
      console.log( this.appInformation.getInformacionCongreso( this.idCongreso ) );
      const rondaActual = this.appInformation.getInformacionCongreso( this.idCongreso )['ronda_inversion_actual'];
      console.log( rondaActual );
      const laboratorioActual = parseInt(  this.userService.getInformationUser().laboratorio, 2 );
      const laboratorio = ( laboratorioActual ).toString();
      this.afs.collection('opcionesInversion')
      .doc(`${this.userService.getInformationUser().idCongreso }-${rondaActual};${laboratorio}-${this.route.snapshot.params['id']}`)
      .set({
        nombreInversion: this.form.value['nameIdea'],
        descripcion: this.form.value['description'],
        impact: this.form.value['impact'],
        opportunity: this.form.value['opportunity'],
        user: this.userService.userName,
        idAreaInversion: `${ this.userService.getIdCongreso() }-${rondaActual.toString() };${laboratorio }`,
        estado: `idea`,
        calificacion: {},
        inversores: [],
        id_grupo: this.route.snapshot.params['id'],
        idCongreso: this.userService.getIdCongreso()

      })
      .then( (value) => {
        this.enaviarIdea = true;
      })
      .catch( error => {
        this.snackBar.open(error, 'Aceptar', {
          duration: 1500,
        });
      });
    }
    this.formSubmitAttempt = true;             // {8}
  }

}
