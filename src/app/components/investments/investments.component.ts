import { Component, OnInit, Inject } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { UserService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { AppServiceService } from 'src/app/services/app-service.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { CalificarIdeaModalComponent } from '../calificar-idea-modal/calificar-idea-modal.component';

@Component({
  selector: 'app-investments',
  templateUrl: './investments.component.html',
  styleUrls: ['./investments.component.css']
})

export class InvestmentsComponent implements OnInit {
  displayedColumns: string[];
  displayedColumnsResults: string[];
  displayedColumnsRankingIdeas: string[];
  displayedColumnsRankingInversores: string[];
  panelOpenState = false;
  rondasInversion: any[] = [];
  private idCongreso: any;

  private rondaInversionActual: any;
  private tematicas: any[] = [];
  private informacinoInversiones: any[] = [];
  private rondaEvaluada = false;
  private valorInversiones: number[][][];
  datosDisponiblesRondasInversion = false;
  usuarioFinal = false;
  usuarioCoordinador = false;
  private informacionApp: any;
  calificada = false;
  private puntosDisponibles: number;

  constructor(
    private afs: AngularFirestore,
    private userService: UserService,
    private router: Router,
    private appInfo: AppServiceService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {
    this.valorInversiones = [];
    this.idCongreso = userService.getIdCongreso();
    this.puntosDisponibles = userService.getInformationUser().puntos_disponibles;
    if (userService.getInformationUser().tipo_usuario.toString() === '3') {
      this.usuarioFinal = true;
      this.usuarioCoordinador = false;
    } else {
      this.usuarioFinal = false;
      this.usuarioCoordinador = true;
    }

    this.informacionApp = this.appInfo.getInformacionCongreso(this.idCongreso);
    this.displayedColumnsRankingIdeas = ['nombreInversion', 'calificacionPromedio', 'clasificacion'];
    this.displayedColumnsResults = ['criterioEvaluacion'];
    this.displayedColumnsRankingInversores = ['nombreInversor', 'valorTotalInversion'];
    if (this.informacionApp['ronda_inversion_actual'] > 1) {
      if (userService.getInformationUser().tipo_usuario === '2') {
        this.displayedColumns = ['nombreInversion', 'impact', 'opportunity', 'actions'];
      } else {
        this.displayedColumns = ['nombreInversion', 'impact', 'opportunity', 'investmentPoints', 'actions'];
      }
    } else if (userService.getInformationUser().tipo_usuario === '2') {
      this.displayedColumns = ['nombreInversion', 'impact', 'opportunity', 'actions'];
    } else {
      this.displayedColumns = ['nombreInversion', 'impact', 'opportunity'];
    }

    this.rondasInversion = appInfo.getRondasInversion(this.idCongreso);
    const valueTematicas = appInfo.getLaboratorios(this.idCongreso);
    // this.tematicas = appInfo.getLaboratorios( userService.getInformationUser().idCongreso );
    if (this.rondasInversion.length > 0) {
      const indexROndaInversionActual = this.informacionApp['ronda_inversion_actual'] - 1;
      this.rondaInversionActual = this.rondasInversion[indexROndaInversionActual];
      const llaveGanadores = `ganadores`;
      if (this.rondaInversionActual.hasOwnProperty(llaveGanadores)) {
        this.rondaEvaluada = true;
      }
      const ri = [];
      const tm = [];
      this.rondasInversion.forEach((item, index) => {
        ri.push({
          nombre: item['nombre'],
          inversiones: []
        });
      });
      valueTematicas.forEach((tematica, indexTm) => {
        this.valorInversiones[indexTm] = [];
        const rnIn = [];
        ri.forEach((rondaIn, indexRonda) => {
          this.afs
            .collection('opcionesInversion',
              ref => ref.where('idAreaInversion', '==', `${this.idCongreso}-${(indexRonda + 1)};${indexTm + 1}`))
            .valueChanges()
            .subscribe(data => {
              this.valorInversiones[indexTm][indexRonda] = [];
              data.forEach((inversion, indexInversion) => {
                this.valorInversiones[indexTm][indexRonda][indexInversion] = 0;
              });
              rnIn.push({
                nombre_ri: ri[indexRonda]['nombre'],
                inversiones: data
              });
            });
        });
        console.log(this.valorInversiones);
        tm.push({
          nombre: tematica['descripcion'],
          rondas_inversion: rnIn
        });
      });
      this.tematicas = tm;
      console.log(this.tematicas);
      this.datosDisponiblesRondasInversion = true;
    }
  }

  ngOnInit() {

  }

  openDialog(inversion: any): void {
    const respuestasCriterios = new Array(this.rondaInversionActual['criterios'].length);
    const partesIdIdea = inversion['idAreaInversion'].toString();
    const idComoPartes = partesIdIdea.split(';');
    const dialogRef = this.dialog.open(CalificarIdeaModalComponent, {
      width: '250px',
      data: {
        criterios: this.rondaInversionActual['criterios'],
        ronda_inversion_actual: this.informacionApp['ronda_inversion_actual'],
        respuestasCriterios: respuestasCriterios
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      let calificacionesRondaActual: any[] = [];
      if (inversion[`ronda_${this.informacionApp['ronda_inversion_actual']}`]) {
        calificacionesRondaActual = inversion[`ronda_${this.informacionApp['ronda_inversion_actual']}`];
      }
      calificacionesRondaActual.push({
        valores: result['respuestasCriterios'],
        calificado_por: this.userService.getInformationUser().correo
      });
      const nombreLlave = new Object();
      nombreLlave['ronda_' + this.informacionApp['ronda_inversion_actual'].toString()] = calificacionesRondaActual;
      this.afs.collection('opcionesInversion')
        .doc(this.idCongreso.toString() + '-1;' + idComoPartes[1].toString() + '-' + inversion['id_grupo'])
        .update(
          nombreLlave
        )
        .then(value => {
          this.calificada = true;
          this.snackBar.open('Se califico correctamento la inversion', 'Aceptar', {
            duration: 1500,
          });
          this.router.navigate(['/coor']);
        }).catch(error => {
          this.snackBar.open(error, 'Aceptar', {
            duration: 1500,
          });
        });
    });
  }

  invertir(
    indiceTematica: number,
    indiceRonda: number,
    indiceInversion: number,
    grupoInversion: string,
    inversores: any
  ) {
    const valorInversion = this.valorInversiones[indiceTematica][indiceRonda][indiceInversion];
    const inversiones = this.userService.getInformationUser().inversiones;
    inversiones.push({
      idInversion: `${this.idCongreso}-1;${indiceTematica + 1}-${grupoInversion}`,
      rondaInversion: (indiceRonda + 1),
      valor: valorInversion
    });

    if (inversores.indexOf(this.userService.userName) === -1) {
      inversores.push(this.userService.userName);
      this.afs
        .collection('opcionesInversion')
        .doc(`${this.idCongreso}-1;${indiceTematica + 1}-${grupoInversion}`)
        .update({
          inversores: inversores
        })
        .then(() => {

          this.userService.actualizarInversiones(inversiones);
          const puntosDisponibles = this.userService.getInformationUser().puntos_disponibles;
          const nuevoPuntajeDisponible = puntosDisponibles - valorInversion;
          this.userService.actualizarPuntajeUsuario(
            this.userService.getInformationUser().puntos_individuales,
            nuevoPuntajeDisponible
          );
          return;

        }).catch(err => {
          console.log(err);
        });
    } else {
      this.userService.actualizarInversiones(inversiones);
      const puntosDisponibles = this.userService.getInformationUser().puntos_disponibles;
      const nuevoPuntajeDisponible = puntosDisponibles - valorInversion;
      this.userService.actualizarPuntajeUsuario(
        this.userService.getInformationUser().puntos_individuales,
        nuevoPuntajeDisponible
      );
    }
    this.snackBar.open('Se ha invertido corectamente ', 'Aceptar', {
      duration: 1500,
    });
  }

}
