import { Component, OnInit, Inject } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatSnackBar } from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import { Router } from '@angular/router';
import { AppServiceService } from 'src/app/services/app-service.service';

export interface DialogData {
  animal: string;
  name: string;
}

export interface Event {
  name: string;
  startTime: string;
  finalHour: string;
}

export interface Laboratorio {
  id: string;
  descripcion: string;
  action: string;
}


@Component({
  selector: 'app-laboratorios',
  templateUrl: './laboratorios.component.html',
  styleUrls: ['./laboratorios.component.css']
})
export class LaboratoriosComponent implements OnInit {

  private laboratorio: any;
  private grupo: String;
  private laboratorios: any[];

  private displayedColumnsLabs: string[] = ['id', 'descripcion', 'action'];
  private dataSourceLabs: Laboratorio[] = [];
  private dataReady = false;

  private ideaEnviada = true;
  animal: string;
  name: string;


  constructor(
    private appService: AppServiceService,
    private userService: UserService,
    private afs: AngularFirestore,
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
    private router: Router

  ) {
    this.laboratorio = ((userService.getInformationUser().laboratorio === '') ? 'vacio' : userService.getInformationUser().laboratorio);
    // tslint:disable-next-line:radix
    this.laboratorio = parseInt( this.laboratorio);
    this.grupo = ((userService.getInformationUser().grupo === '') ? 'vacio' : userService.getInformationUser().grupo);
    this.ideaEnviada = userService.getIdeaEnviada();
    this.laboratorios = appService.getLaboratorios( userService.getInformationUser().idCongreso );
  }

  ngOnInit() {
  }

  entrarAlLaboratorio( idLaboratorio: number ) {
    this.afs.doc( 'usuarios/' + this.userService.userName.toString())
      .update({
        'laboratorio': idLaboratorio.toString()
      }).then( value => {
        this.snackBar.open('Inscripcion realizada exitosamente', 'Aceptar', {
          duration: 1500,
        });
      });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialogComponent, {
      width: '250px',
      data: {name: this.name, animal: this.grupo}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.afs.collection('usuarios')
      .doc(this.userService.userName)
      .update({
        grupo: result.toString()
      }).then(value => {
        this.afs.collection('opcionesInversion', ref => ref.where('id_grupo', '==', this.grupo.toString()))
        .valueChanges()
        .subscribe( data => {
          if ( data.length > 0 ) {
            this.ideaEnviada = true;
          }
          this.userService.updateInformationUser();
          this.snackBar.open('Inscripcion realizada exitosamente', 'Aceptar', {
            duration: 1500,
          });
        });
      }).catch( error => {
        this.snackBar.open(error, 'Aceptar', {
          duration: 1500,
        });
      });
    });
  }

  entrarAlGrupo() {
    this.router.navigate(['../design_thinking', this.grupo]);
  }
}

@Component({
  selector: 'app-dialog-overview-example-dialog',
  templateUrl: 'dialog-overview-example-dialog.html',
})
export class DialogOverviewExampleDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
