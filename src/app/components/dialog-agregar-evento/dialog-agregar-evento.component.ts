import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-agregar-evento',
  templateUrl: './dialog-agregar-evento.component.html',
  styleUrls: ['./dialog-agregar-evento.component.css']
})

export class DialogAgregarEventoComponent {

  constructor(
    public dialogRef: MatDialogRef<DialogAgregarEventoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Object) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  toggleMeridian() {
    this.data['meridiano'] = !this.data['meridiano'];
  }
}
