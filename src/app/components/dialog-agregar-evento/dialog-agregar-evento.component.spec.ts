import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogAgregarEventoComponent } from './dialog-agregar-evento.component';

describe('DialogAgregarEventoComponent', () => {
  let component: DialogAgregarEventoComponent;
  let fixture: ComponentFixture<DialogAgregarEventoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogAgregarEventoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogAgregarEventoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
