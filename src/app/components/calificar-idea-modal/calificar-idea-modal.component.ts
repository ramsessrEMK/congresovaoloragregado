import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-calificar-idea-modal',
  templateUrl: './calificar-idea-modal.component.html',
  styleUrls: ['./calificar-idea-modal.component.css']
})
export class CalificarIdeaModalComponent {

  constructor(
    public dialogRef: MatDialogRef<CalificarIdeaModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Object) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
