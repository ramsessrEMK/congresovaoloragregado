import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearOpcionesInversionComponent } from './crear-opciones-inversion.component';

describe('CrearOpcionesInversionComponent', () => {
  let component: CrearOpcionesInversionComponent;
  let fixture: ComponentFixture<CrearOpcionesInversionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearOpcionesInversionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearOpcionesInversionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
