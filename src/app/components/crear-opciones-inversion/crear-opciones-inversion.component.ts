import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AppServiceService } from 'src/app/services/app-service.service';
import { UserService } from 'src/app/services/user.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-crear-opciones-inversion',
  templateUrl: './crear-opciones-inversion.component.html',
  styleUrls: ['./crear-opciones-inversion.component.css']
})
export class CrearOpcionesInversionComponent implements OnInit {

  form: FormGroup;                    // {1}
  private formSubmitAttempt: boolean; // {2}
  private appInfo: any;
  private enviarOpcionInversion = false;
  private tematicas: any;
  constructor(
    private fb: FormBuilder,
    private afs: AngularFirestore,
    private router: Router,
    private route: ActivatedRoute,
    private appInformation: AppServiceService,
    private userService: UserService,
    private snackBar: MatSnackBar
  ) {
    const that = this;

    appInformation.getAppInfo()
    .subscribe( value => {
      this.appInfo = value;
      afs.doc(`rondas_inversion/${ value['ronda_inversion_actual'] }`)
        .valueChanges()
        .subscribe((data) => {
          this.tematicas = data['temáticas'];
          console.log( this.tematicas );
      });
    });
  }

  ngOnInit() {
    this.form = this.fb.group({     // {5}
      nameIdea: ['', Validators.required],
      opportunity: ['', Validators.required],
      description: ['', Validators.required],
      impact: ['', Validators.required]
      // areaInversion: ['', Validators.required]
    });
  }

  isFieldInvalid(field: string) { // {6}
    return (
      (!this.form.get(field).valid && this.form.get(field).touched) ||
      (this.form.get(field).untouched && this.formSubmitAttempt)
    );
  }

  onSubmit() {
    if (this.form.valid) {

      const rondaActual = this.appInformation.getInformationApp()['ronda_inversion_actual'];
      // tslint:disable-next-line:radix
      const laboratorioActual = parseInt(  this.userService.getInformationUser().laboratorio.toString() );
      const laboratorio = ( laboratorioActual - 1).toString();
      this.afs.collection('opcionesInversion')
      .doc(`${rondaActual };${laboratorio }--1`)
      .set({
        nombreInversion: this.form.value['nameIdea'],
        descripcion: this.form.value['description'],
        impact: this.form.value['impact'],
        opportunity: this.form.value['impact'],
        user: this.userService.userName,
        idAreaInversion: `${rondaActual };${laboratorio }`,
        estado: `inversion`,
        inversores: [],
        calificacion_primera_ronda: [],
        calificacion_segunda_ronda: [],
        calificacion_tercera_ronda: [],
        id_grupo: '-1'
      })
      .then( (value) => {
        this.enviarOpcionInversion = true;
      })
      .catch( error => {
        this.snackBar.open(error, 'Aceptar', {
          duration: 1500,
        });
      });
    }

    this.formSubmitAttempt = true;             // {8}
  }

}
