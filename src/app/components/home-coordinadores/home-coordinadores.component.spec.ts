import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeCoordinadoresComponent } from './home-coordinadores.component';

describe('HomeCoordinadoresComponent', () => {
  let component: HomeCoordinadoresComponent;
  let fixture: ComponentFixture<HomeCoordinadoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeCoordinadoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeCoordinadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
