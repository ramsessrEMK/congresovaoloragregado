import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-nuevo-criterio-inversion',
  templateUrl: './nuevo-criterio-inversion.component.html',
})
export class NuevoCriterioInversionComponent {

  constructor(
    public dialogRef: MatDialogRef<NuevoCriterioInversionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Object
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
