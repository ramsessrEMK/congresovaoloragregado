import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NuevoCriterioInversionComponent } from './nuevo-criterio-inversion.component';

describe('NuevoCriterioInversionComponent', () => {
  let component: NuevoCriterioInversionComponent;
  let fixture: ComponentFixture<NuevoCriterioInversionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevoCriterioInversionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevoCriterioInversionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
