import { Component, OnInit } from '@angular/core';
import { AppServiceService } from 'src/app/services/app-service.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatSnackBar, MatDialog } from '@angular/material';
import { parse } from 'url';
import * as firebase from 'firebase';
import { AdministrarUsuariosService } from 'src/app/services/administrar-usuarios.service';
import {
  DialogoCrearNuevaRondaInversionComponent
} from '../dialogo-crear-nueva-ronda-inversion/dialogo-crear-nueva-ronda-inversion.component';
import { CrearCongresoComponent } from '../crear-congreso/crear-congreso.component';
import { Router } from '@angular/router';
import { element } from '@angular/core/src/render3/instructions';

@Component({
  selector: 'app-home-admin',
  templateUrl: './home-admin.component.html',
  styleUrls: ['./home-admin.component.css']
})
export class HomeAdminComponent implements OnInit {

  /*
  private appInfo: any;
  private informacionEstadoApp = false;
  private podiumRondaInversion: any[] = [];
  private evaluarResultados = true;
  // private rondasInversion: any[] = [];
  private inversoresGanadores: any;
  private evaluacionOpcionesInversion: any;
  private tematicas: any[];

  private aplicaciones: any[] = [];
  */
  // ---------------------------------------

  private informacionCongresos: any[] = [];
  private rondaInversionActual: any;
  private permitirCerrarRondaInversion = false;
  private visualizarBotonGanadores = false;

  constructor(
    private appService: AppServiceService,
    private afs: AngularFirestore,
    private snackBar: MatSnackBar,
    private adminUsers: AdministrarUsuariosService,
    private dialog: MatDialog,
    private router: Router
  ) {
    this.informacionCongresos = appService.getInformacionCongresos();
    /*
    this.appInfo = this.appService.getInformationApp();
    this.rondaInversionActual = this.appInfo['ronda_inversion_actual'];
    this.afs
      .collection('rondas_inversion')
      .valueChanges()
      .subscribe(valueRondasInversion => {
        this.rondasInversion = valueRondasInversion;
        this.informacionEstadoApp = true;
        this.visualizarGanadores();
      });
    */
  }

  ngOnInit() {
  }

  public abrirDialogoNuevoCongreso(): void {
    const nombreNuevoCongreso = '';
    const dialogRef = this.dialog.open(CrearCongresoComponent, {
      width: '250px',
      data: {
        nombreNuevoCongreso: nombreNuevoCongreso,
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined && result.nombreNuevoCongreso.trim() !== '') {
        const nuevoCongreso = {
          nombre_congreso: result.nombreNuevoCongreso,
          ronda_inversion_actual: 1
        };
        this.informacionCongresos.push(nuevoCongreso);
        const nuevoCalendario = {
          idCongreso: this.informacionCongresos.length,
          calendario: []
        };
        this.appService.crearNuevoCalendario(nuevoCalendario, this.informacionCongresos.length);
        this.appService.crearNuevoCongreso(nuevoCongreso, this.informacionCongresos.length);
        this.abrirDialogoNuevaRondaInversion(this.informacionCongresos.length);
      }
    });
  }

  public abrirDialogoNuevaRondaInversion(idCongreso): void {
    const nombreNuevaRondaInversion = '';
    const numeroGanadores = 1;
    const dialogRef = this.dialog.open(DialogoCrearNuevaRondaInversionComponent, {
      width: '250px',
      data: {
        nombreRondaInversion: nombreNuevaRondaInversion,
        numeroGanadores: numeroGanadores
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      const rondasInversion = this.appService.getRondasInversion(idCongreso);
      const nuevaRondaInversion = {
        criterios: [],
        ganadores: [],
        nombre: result.nombreRondaInversion,
        numero_ganadores: result.numeroGanadores,
        tematicas: [],
        idCongreso: idCongreso
      };
      rondasInversion.push(nuevaRondaInversion);
      this.appService.crearRondaInversion(nuevaRondaInversion, rondasInversion.length, idCongreso);
    });
  }

  visualizarGanadores(idCongreso) {
    const informacionCongreso = this.appService.getInformacionCongreso(idCongreso + 1);
    const rondasInversion = this.appService.getRondasInversion((idCongreso + 1));
    const infoRondaInversionActual = rondasInversion[(informacionCongreso['ronda_inversion_actual'] - 1)];
    const numeroGanadores = infoRondaInversionActual['numero_ganadores'];
    const tematicas = this.appService.getLaboratorios(idCongreso + 1);
    this.rondaInversionActual = informacionCongreso['ronda_inversion_actual'];
    tematicas.forEach((tematica, index) => {
      this.afs
        .collection(
          'opcionesInversion',
          ref => ref.where('idAreaInversion', '==', `${(idCongreso + 1)}-${this.rondaInversionActual};${index + 1}`)
        )
        .valueChanges()
        .subscribe(inversiones => {
          const inversionesCalificadas = inversiones.filter(
            inversion => inversion[`ronda_${this.rondaInversionActual}`]
          );

          if (inversionesCalificadas.length >= numeroGanadores) {
            inversionesCalificadas.forEach((inversion, indexInversion) => {
              const calificaciones = inversion[`ronda_${this.rondaInversionActual}`];
              let valorTotal = 0;
              let calificacion: number;
              for (calificacion = 0; calificacion < calificaciones.length; calificacion++) {
                let valores: number;
                let valorCalificacion = 0;
                if (calificaciones[calificacion]['valores']) {
                  for (valores = 0; valores < calificaciones[calificacion]['valores'].length; valores++) {
                    valorCalificacion += parseFloat(calificaciones[calificacion]['valores'][valores]);
                  }
                  valorCalificacion = valorCalificacion / calificaciones[calificacion]['valores'].length;
                }
                valorTotal += valorCalificacion;
              }
              valorTotal = valorTotal / calificaciones.length;
              inversionesCalificadas[indexInversion][`calificacion_final_ronda_${this.rondaInversionActual}`] = valorTotal;

            });
            const that = this;
            inversionesCalificadas.sort(function (a, b) {
              if (
                a[`calificacion_final_ronda_${that.rondaInversionActual}`] >
                b[`calificacion_final_ronda_${that.rondaInversionActual}`]
              ) {
                return 1;
              }
              if (
                a[`calificacion_final_ronda_${that.rondaInversionActual}`] <
                b[`calificacion_final_ronda_${that.rondaInversionActual}`]
              ) {
                return -1;
              }
              // a must be equal to b
              return 0;
            });
            const ideasGanadoras = inversionesCalificadas.slice((numeroGanadores * -1));
            ideasGanadoras.reverse();
            let podiumRondaInversion: any[];
            if (this.informacionCongresos[idCongreso]['calificacion'] === undefined) {
              podiumRondaInversion = [];
            } else {
              podiumRondaInversion = this.informacionCongresos[idCongreso]['calificacion'];
            }


            if (this.rondaInversionActual === rondasInversion.length) {
              ideasGanadoras.forEach(ideaGanadora => {
                podiumRondaInversion.push(ideaGanadora);
              });
            } else {
              podiumRondaInversion.push(ideasGanadoras);
            }
            this.informacionCongresos[idCongreso]['calificacion'] = podiumRondaInversion;
            console.log(this.informacionCongresos);
            if (informacionCongreso['ronda_inversion_actual'] > 1) {
              this.inversores(idCongreso);
            }
            this.visualizarBotonGanadores = true;
          } else {
            // this.podiumRondaInversion.push([]);
            this.snackBar.open('No hay inversiones calificadas para evaluar en la tematica: ' + tematica, 'Aceptar', {
              duration: 1500,
            });
          }
        }

        );
    });
  }

  evaluarInversores(inversion, idCongreso) {
    const idInversion = inversion['idAreaInversion'];
    const idInversionComoArray = idInversion.split(';');
    inversion['inversores'].forEach((inversor, indexInversor) => {
      this.afs
        .collection('usuarios')
        .doc(inversor)
        .valueChanges()
        .subscribe(infoInversor => {
          const infoInversiones = infoInversor['inversiones'];
          const estaInversion = infoInversiones.filter(elemento => {
            return elemento['idInversion'] === `${(idCongreso + 1)}-1;${idInversionComoArray[1]}-${inversion['id_grupo']}`;
          });
          let valorTotal = 0;
          estaInversion.forEach((valorInversion, indexValorInversion) => {
            // tslint:disable-next-line:radix
            valorTotal += parseInt(valorInversion['valor']);
          });
          const puntos_individuales = infoInversor['puntos_individuales'];
          const nuevoTotalPuntos = puntos_individuales + valorTotal;
          infoInversor['puntos_individuales'] = nuevoTotalPuntos;
          infoInversor['puntos_disponibles'] = nuevoTotalPuntos;
          infoInversor['valorTotalInversion'] = valorTotal;
          infoInversor['idInversor'] = inversor;
          this.informacionCongresos[idCongreso]['ganadores'].push(infoInversor);
          console.log( this.informacionCongresos[idCongreso]['ganadores'] );
        });
    });
  }

  inversores(idCongreso) {
    const informacionCongreso = this.appService.getInformacionCongreso(idCongreso + 1);
    const rondasInversion = this.appService.getRondasInversion(idCongreso + 1);
    this.informacionCongresos[idCongreso]['ganadores'] = [];
    this.informacionCongresos[idCongreso]['calificacion'].forEach((inversion, indexInversion) => {
      if (informacionCongreso['ronda_inversion_actual'] < rondasInversion.length) {
        inversion.forEach(inver => { this.evaluarInversores(inver, idCongreso); });
      } else {
        this.evaluarInversores(inversion, idCongreso);
      }
    });
  }

  publicarResultados(idCongreso) {
    const informacionCongreso = this.appService.getInformacionCongreso(idCongreso + 1);
    const rondasInversion = this.appService.getRondasInversion(idCongreso + 1);
    let ideasGanadoras = [];
    if (this.rondaInversionActual < rondasInversion.length) {
      ideasGanadoras = this.informacionCongresos[idCongreso]['calificacion'].map((ronda, index) => {
        return {
          idRondaInversion: (index + 1),
          ideas: ronda
        };
      });
    } else {
      ideasGanadoras = this.informacionCongresos[idCongreso]['calificacion'];
    }

    if (this.rondaInversionActual === 1) {
      this.afs
        .collection('rondas_inversion')
        .doc(`${(idCongreso + 1)};${this.rondaInversionActual.toString()}`)
        .update({
          ganadores: firebase.firestore.FieldValue.delete()
        })
        .then(value => {
          this.afs
            .collection('rondas_inversion')
            .doc(`${(idCongreso + 1)};${this.rondaInversionActual.toString()}`)
            .update({
              ganadores: ideasGanadoras
            })
            .then(valorGanadores => {
              if (informacionCongreso['ronda_inversion_actual'] < rondasInversion.length) {
                this.permitirCerrarRondaInversion = true;
              }
              this.snackBar.open('Se actualizaron correctamente los datos', 'Aceptar', {
                duration: 1500,
              });

            });
        });
    } else {
      this.afs
        .collection('rondas_inversion')
        .doc(`${(idCongreso + 1)};${this.rondaInversionActual.toString()}`)
        .update({
          ganadores: firebase.firestore.FieldValue.delete(),
          inversoresGanadores: firebase.firestore.FieldValue.delete()
        })
        .then(value => {
          this.afs
            .collection('rondas_inversion')
            .doc(`${(idCongreso + 1)};${this.rondaInversionActual.toString()}`)
            .update({
              ganadores: ideasGanadoras,
              inversoresGanadores: this.informacionCongresos[idCongreso]['ganadores']
            })
            .then(valorGanadores => {
              if (informacionCongreso['ronda_inversion_actual'] < rondasInversion.length) {
                this.permitirCerrarRondaInversion = true;
              }
              this.snackBar.open('Se actualizaron correctamente los datos', 'Aceptar', {
                duration: 1500,
              });
            });
        });
    }
  }

  cerrarRondaInversion(idCongreso) {
    if (this.rondaInversionActual === this.appService.getRondasInversion((idCongreso + 1)).length) {
      this.snackBar.open('La ronda actual es la ultima ronda definida no se puede pasar a una siguiente ronda', 'Aceptar', {
        duration: 1500,
      });
      return;
    } else {
      const rondaInversionActual = this.appService.getInformacionCongreso(idCongreso + 1)['ronda_inversion_actual'];
      if (rondaInversionActual > 1) {
        this.adminUsers.updateUsuariosInversionGanada(this.informacionCongresos[idCongreso]['ganadores']);
      }
      this.visualizarBotonGanadores = false;
      this.informacionCongresos[idCongreso]['calificacion'].forEach((ideaGanadora, index) => {
        if (rondaInversionActual === this.appService.getRondasInversion(idCongreso + 1).length) {
          this.actualizarInversion(ideaGanadora, idCongreso);
        } else {
          ideaGanadora.forEach((ganador, indexGanador) => {
            this.actualizarInversion(ganador, idCongreso);
          });
        }
      });
      this.appService.cambiaRondaInversion((idCongreso + 1));
    }
  }

  private actualizarInversion(ideaGanadora, idCongreso) {
    const rondaInversionActual = this.appService.getInformacionCongreso(idCongreso + 1)['ronda_inversion_actual'];
    const idAreaInversion = ideaGanadora['idAreaInversion'];
    const areaInversionAsArray = idAreaInversion.split(';');
    const idGrupo = ideaGanadora['id_grupo'];
    console.log(`${(idCongreso + 1)}-1;${areaInversionAsArray[1]}-${idGrupo}`);
    this.afs
      .collection('opcionesInversion')
      .doc(`${(idCongreso + 1)}-1;${areaInversionAsArray[1]}-${idGrupo}`)
      .update({
        idAreaInversion: `${(idCongreso + 1)}-${rondaInversionActual + 1};${areaInversionAsArray[1]}`
      })
      .then(value => {
        // this.informacionCongresos[idCongreso]['ronda_inversion_actual']++;
      });
  }
  /*
  agregarTematica() {

    this.afs
        .collection('rondas_inversion')
        .doc(this.rondaInversionActual.toString())
        .update({
          ganadores: firebase.firestore.FieldValue.delete()
        })
        .then(value => {
          this.afs
            .collection('rondas_inversion')
            .doc(this.rondaInversionActual.toString())
            .update({
              ganadores: this.podiumRondaInversion[(this.rondaInversionActual - 1)]
            })
            .then(valorGanadores => {
              this.permitirCerrarRondaInversion = true;
              this.snackBar.open('Se actualizaron correctamente los datos', 'Aceptar', {
                duration: 1500,
              });

            });
        });

  }
  */
}
