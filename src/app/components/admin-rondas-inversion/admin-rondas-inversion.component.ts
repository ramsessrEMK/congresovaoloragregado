import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AppServiceService } from 'src/app/services/app-service.service';
import {MatDialog, MatSnackBar} from '@angular/material';
import { NuevoCriterioInversionComponent } from '../nuevo-criterio-inversion/nuevo-criterio-inversion.component';

@Component({
  selector: 'app-admin-rondas-inversion',
  templateUrl: './admin-rondas-inversion.component.html',
  styleUrls: ['./admin-rondas-inversion.component.css']
})
export class AdminRondasInversionComponent implements OnInit {

  private informacionLista = false;
  private appInfo: any;
  private infoRondaInversion: any;
  private nombreNuevoCriterio: String;
  private rondaInversionEvaluar: string;
  constructor(
    private route: ActivatedRoute,
    private afs: AngularFirestore,
    private appService: AppServiceService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {
    this.nombreNuevoCriterio = '';
  }

  ngOnInit() {
    this.appInfo = this.appService.getInformationApp();
    this.rondaInversionEvaluar = this.route.snapshot.params['id'];
    this.afs
        .collection('rondas_inversion')
        .doc( this.rondaInversionEvaluar )
        .valueChanges()
        .subscribe(rondaInversion => {
          this.infoRondaInversion = rondaInversion;
          this.informacionLista = true;
        });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(NuevoCriterioInversionComponent, {
      width: '250px',
      data: {
        nombreCriterio: this.nombreNuevoCriterio
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if ( result !== undefined && result.nombreCriterio.trim() !== ''  ) {
        this.infoRondaInversion['criterios'].push(result.nombreCriterio);
        this.afs
            .collection('rondas_inversion')
            .doc(this.rondaInversionEvaluar)
            .update({
              criterios: this.infoRondaInversion['criterios']
            })
            .then( value => {
              this.snackBar.open( 'Se agrego el nuevo criterio exitosamente', 'Aceptar', {
                duration: 1500
              });
            })
            .catch( err => {
              console.log( err );
            });
      }
    });
  }
}
