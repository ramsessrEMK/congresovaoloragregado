import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-crear-nuevo-usuario',
  templateUrl: './dialog-crear-nuevo-usuario.component.html',
  styleUrls: ['./dialog-crear-nuevo-usuario.component.css']
})

export class DialogCrearNuevoUsuarioComponent {

  private laboratorios: any[] = [];
  constructor(
    public dialogRef: MatDialogRef<DialogCrearNuevoUsuarioComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Object
  ) {
    dialogRef.disableClose = true;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  obtenerLaboratorios(): void {
    console.log(this.data['idCongreso']);
    this.laboratorios = this.data['appService'].getLaboratorios( parseInt(this.data['idCongreso'], 2) );
    console.log(this.laboratorios);
  }

}
