import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AppServiceService } from 'src/app/services/app-service.service';
import { DialogAgregarEventoComponent } from '../dialog-agregar-evento/dialog-agregar-evento.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-administrar-agenda',
  templateUrl: './administrar-agenda.component.html',
  styleUrls: ['./administrar-agenda.component.css']
})
export class AdministrarAgendaComponent implements OnInit {

  private idCongreso: any;
  private informacionCalendario: any[];
  constructor(
    private route: ActivatedRoute,
    private appService: AppServiceService,
    private dialog: MatDialog,
  ) {
    this.idCongreso = this.route.snapshot.params['id'];
    const calendario = appService.getCalendario( parseInt( this.idCongreso, 2 ) );
    this.informacionCalendario = calendario[ ( parseInt( this.idCongreso, 2 ) - 1 ) ].calendario;
  }

  ngOnInit() {
  }

  public abrirDialogoNuevoEvento(): void {
    const horarioDesde = {hour: 8, minute: 0};
    const horarioHasta = {hour: 8, minute: 30};
    const meridiano = false;
    const descripcion = '';
    const esLaboratorio = false;
    const dialogRef = this.dialog.open(DialogAgregarEventoComponent, {
      width: '250px',
      data: {
        horarioDesde: horarioDesde,
        horarioHasta: horarioHasta,
        meridiano: meridiano,
        descripcion: descripcion,
        esLaboratorio: esLaboratorio
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log( result.horarioDesde );
      const informacionNuevoEvento = {
        horario: `${result.horarioDesde.hour}:${result.horarioDesde.minute} - ${result.horarioHasta.hour}:${result.horarioHasta.minute}`,
        descripcion: result.descripcion,
        esLaboratorio: result.esLaboratorio,
      };
      this.informacionCalendario.push(informacionNuevoEvento);
      this.appService.actualizarCalendario(this.informacionCalendario, this.idCongreso);
    });
  }
}
