import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { BehaviorSubject, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { User } from '../auth/user';
import { Usuario } from '../models/usuario';
import { MatSnackBar } from '@angular/material';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  private ideaEnviada: boolean;

  private userInfo: Usuario;
  public tasks: Observable<{}[]>;
  private taskDoc: AngularFirestoreDocument<{}>;
  private infoUser$: BehaviorSubject<String>;
  private queryObservable: Observable<{}[]>;
  public email: String;
  public grupo: String;
  public laboratorio: String;
  public puntosIndividuales: Number;
  public puntosTotales: Number;
  public userName: string;
  constructor(
    public db: AngularFirestore,
    public afs: AngularFirestore,
    private snackBar: MatSnackBar
  ) {
    this.updateInformationUser();
  }

  updateInformationUser() {
    this.infoUser$ = new BehaviorSubject<String>('');
    this.queryObservable = this.infoUser$.pipe(
      switchMap((email) =>
        this.db.collection('usuarios', ref => {
          let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
          query = query.where('correo', '==', email);
          return query;
        }).valueChanges()
      )
    );
  }

  actualizarPuntajeUsuario(nuevoPuntaje: number, puntosDisponibles: number) {
    this.afs
      .collection('usuarios')
      .doc(this.userName)
      .update({
        puntos_individuales: nuevoPuntaje,
        puntos_disponibles: puntosDisponibles
      })
      .then(() => {
        this.snackBar.open('Puntaje actualizado correctamente', 'Aceptar', {
          duration: 1500,
        });
      })
      .catch(err => {
        this.snackBar.open('Hub un error al actualizar el puntaje', 'Aceptar', {
          duration: 1500,
        });
      });
  }

  actualizarInversiones(inversiones: Object[]) {
    this.afs
      .collection('usuarios')
      .doc(this.userName)
      .update({
        inversiones: inversiones
      })
      .then(() => {
        this.snackBar.open('Inversiones actualizadas correctamente', 'Aceptar', {
          duration: 1500,
        });
      })
      .catch(err => {
        this.snackBar.open('Hub un error al actualizar las inversiones', 'Aceptar', {
          duration: 1500,
        });
      });
  }

  setInformationUser(user: Usuario) {
    this.userInfo = user;
    if (this.userInfo.laboratorio !== '' && this.userInfo.grupo !== '') {
      this.afs.collection(
        'opcionesInversion',
        ref => ref.where('id_grupo', '==', this.userInfo.grupo)
      )
        .valueChanges()
        .subscribe(data => {
          if (data.length === 0) {
            this.ideaEnviada = false;
          } else {
            this.ideaEnviada = true;
          }
        });
    } else {
      this.ideaEnviada = false;
    }
  }

  getIdCongreso( ) {
    return this.getInformationUser().idCongreso;
  }

  getInformationUser() {
    return this.userInfo;
  }

  getIdeaEnviada() {
    return this.ideaEnviada;
  }

  setIdeaEnviada(estado: boolean) {
    this.ideaEnviada = estado;
  }

}
