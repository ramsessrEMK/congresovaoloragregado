import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UsuarioId } from '../models/usuario-id';
import { Usuario } from '../models/usuario';
import { AngularFireAuth } from '@angular/fire/auth';
import { MatSnackBar, MatDialog } from '@angular/material';
import { DialogCrearNuevoUsuarioComponent } from '../components/dialog-crear-nuevo-usuario/dialog-crear-nuevo-usuario.component';
import { AppServiceService } from './app-service.service';

@Injectable({
  providedIn: 'root'
})
export class AdministrarUsuariosService {
  private collecionUsuarios: AngularFirestoreCollection;
  private usuarios: Observable<UsuarioId[]>;
  private informacionUsuarios: UsuarioId[] = [];
  constructor(
    private readonly afs: AngularFirestore,
    private authFirebase: AngularFireAuth,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
  ) {
    this.collecionUsuarios = afs.collection<Usuario>('usuarios');
    this.usuarios = this.collecionUsuarios.snapshotChanges().pipe(
      map(acciones => acciones.map(a => {
        const data = a.payload.doc.data() as Usuario;
        const id = a.payload.doc.id;
        const usuario = { id, ...data };
        this.informacionUsuarios.push(usuario);
        return usuario;
      }))
    );
  }

  public crearNuevoUsuario(primerUaurio: boolean, congresos: any[], appInformation: AppServiceService): void {
    const nombre = '';
    const tipoUsuario = '';
    const identificacion = '';
    const correo = '';
    const idCongreso = 0;
    const laboratorio = '';
    const grupo = '';
    const dialogRef = this.dialog.open(DialogCrearNuevoUsuarioComponent, {
      width: '400px',
      data: {
        appService: appInformation,
        nombre: nombre,
        identificacion: identificacion,
        correo: correo,
        primerUsuario: primerUaurio,
        tipoUsuario: tipoUsuario,
        congresos: congresos,
        idCongreso: idCongreso,
        laboratorio: laboratorio,
        grupo: grupo
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.agregarUsuario(
          result.correo,
          result.identificacion.toString(),
          result.identificacion.toString(),
          result.nombre,
          result.tipoUsuario,
          (result.idCongreso === undefined) ? 0 : parseInt( result.idCongreso, 2 ),
          (result.laboratorio === undefined) ? '' : result.laboratorio,
          (result.grupo === undefined) ? '' : result.grupo
        );
      }
    });
  }

  getInformacionUsuario(): UsuarioId[] {
    return this.informacionUsuarios;
  }

  getCollecionUsuarios(): AngularFirestoreCollection {
    return this.collecionUsuarios;
  }

  getUsuarios(): Observable<UsuarioId[]> {
    return this.usuarios;
  }

  updateUsuarios() {
    this.usuarios.forEach((listaUsuarios) => {
      listaUsuarios.forEach(usuario => {
        if (usuario.tipo_usuario === '3') {
          usuario.puntos_individuales += 1000;
          this.afs
            .collection('usuarios')
            .doc(usuario.id)
            .update({
              puntos_individuales: usuario.puntos_individuales
            });
        }
      });
    });
  }

  updateUsuariosInversionGanada(usuariosActualizar: any) {

    usuariosActualizar.forEach((usuario, indexUsuario) => {
      this.afs
        .collection('usuarios')
        .doc(usuario['idInversor'])
        .update({
          puntos_disponibles: usuario['puntos_disponibles'],
          puntos_individuales: usuario['puntos_individuales']
        })
        .then(() => {
          console.log('actualizado puntaje de usuario');
          return;
        });
    });
  }

  /*
  registrarUsuariosParticipantes() {
    const contraseñas = [];
    const regitro = this.textoAlmacenamientoMasivoUsuarioParticipante.split('\n');
    regitro.forEach(value => {
      const informacion = value.split(';');
      this.agregarUsuarioParticipante(informacion[3], informacion[2], informacion[2], `${informacion[0]}  ${informacion[1]} `);
    });
  }

  registrarUsuariosCoordinador() {
    const regitro = this.textoAlmacenamientoMasivoUsuarioParticipante.split('\n');
    regitro.forEach(value => {
      const informacion = value.split(';');
      this.agregarUsuarioCoordinador(informacion[3], informacion[2], informacion[2], `${informacion[0]}  ${informacion[1]} `);
    });
  }
  */

  registrarEnBd(email, password, username, nombre, tipoUsuario, idCongreso, laboratorio, grupo) {
    this.afs.collection('usuarios').doc(username.toString()).set({
      nombre: nombre,
      correo: email,
      grupo: grupo,
      laboratorio: laboratorio,
      puntos_disponibles: 0,
      puntos_individuales: 0,
      puntos_invertidos: [],
      inversiones: [],
      tipo_usuario: tipoUsuario,
      idCongreso: idCongreso
    })
      .then(() => {
        this.snackBar.open('Usuario registrado exitosamente', 'Aceptar', {
          duration: 1500,
        });
        return;
      })
      .catch(error => {
        this.snackBar.open('Error al registrar el usuario', 'Aceptar', {
          duration: 1500,
        });
        return;
      });
  }

  agregarUsuario(email, password, username, nombre, tipoUsuario, idCongreso, laboratorio, grupo) {
    this.authFirebase.auth.createUserWithEmailAndPassword(email, password.toString())
      .then(value => {
        this.registrarEnBd(email, password, username, nombre, tipoUsuario, idCongreso, laboratorio, grupo);
      })
      .catch(error => {
        this.snackBar.open('Error al registrar el usuario', 'Aceptar', {
          duration: 1500,
        });
        return;
      });
  }
}
