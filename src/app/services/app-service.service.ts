import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { MatSnackBar, MatDialog } from '@angular/material';
import * as firebase from 'firebase';
import { AdministrarUsuariosService } from './administrar-usuarios.service';
@Injectable({
  providedIn: 'root'
})
export class AppServiceService {
  private informacionApllicacionApp: any;
  private rondasInversion: any[];
  private calendarios: any[];
  private appInfo: any;
  private rondaInversion: any;
  public information: any[] = [];
  private informacion: any[] = [];
  private aplicacionInicializada: boolean;
  constructor(
    private afs: AngularFirestore,
    private snackBar: MatSnackBar,
    private administrarusuarios: AdministrarUsuariosService
  ) {

    this.afs
      .collection('usuarios')
      .valueChanges()
      .subscribe(value => {
        if (value.length === 0) {
          administrarusuarios.crearNuevoUsuario( true, this.informacion, this );
        }
      });

    this.afs
      .collection('calendarios')
      .valueChanges()
      .subscribe(value => {
        this.calendarios = value;
      });

    this.afs
      .collection('rondas_inversion')
      .valueChanges()
      .subscribe(value => {
        this.rondasInversion = value;
      });

    this.informacionApllicacionApp = this.afs.collection('aplicación').valueChanges();
    this.informacionApllicacionApp.subscribe(value => {
      this.informacion = value;
    });

    this.appInfo = this.afs.doc('aplicación/congreso').valueChanges();
    this.appInfo.subscribe(value => {
      this.information = value;

    });
  }

  getAplicacionInicializada() {
    return this.aplicacionInicializada;
  }

  actualizarCalendario(calendario, idCongreso) {
    this.afs
      .collection('calendarios')
      .doc(idCongreso)
      .update({
        calendario: firebase.firestore.FieldValue.delete()
      })
      .then(value => {
        this.afs
          .collection('calendarios')
          .doc(idCongreso)
          .update({
            calendario: calendario
          })
          .then(valorGanadores => {
            this.snackBar.open('Se actualizaron correctamente los datos', 'Aceptar', {
              duration: 1500,
            });

          });
      });
  }

  getCalendario(idCongreso) {
    return this.calendarios.filter(calendario => calendario['idCongreso'] === idCongreso);
  }

  getLaboratorios(idCongreso) {
    const calendario = this.getCalendario(idCongreso);
    if ( calendario.length === 0 ) {
      return calendario;
    } else {
      return calendario[0]['calendario'].filter(evento => evento['esLaboratorio'] === true);
    }

  }



  getRondasInversion(idCongreso) {
    return this.rondasInversion.filter(rondaInversion => rondaInversion['idCongreso'] === idCongreso);
  }

  crearNuevoCongreso(informacionCongreso: Object, idCongreso: number) {
    this.afs
      .collection('aplicación')
      .doc(idCongreso.toString())
      .set(informacionCongreso)
      .then(() => {
        this.snackBar.open('Se ha creado el nuevo congreso correctamente', 'Aceptar', {
          duration: 1500,
        });
      });
  }

  crearRondaInversion(informacionRondaInversion: any, idRondaInversion: number, idCongreso: number) {
    this.afs
      .collection('rondas_inversion')
      .doc(`${idCongreso};${idRondaInversion}`)
      .set(informacionRondaInversion)
      .then(() => {
        this.snackBar.open('Se ha creado la ronda de inversion correctamente', 'Aceptar', {
          duration: 1500,
        });
      });
  }

  crearNuevoCalendario(informacionNuevoCalendario: Object, idCongreso: number) {
    this.afs
      .collection('calendarios')
      .doc(`${idCongreso}`)
      .set(informacionNuevoCalendario)
      .then(() => {
        this.snackBar.open('Se ha creado la ronda de inversion correctamente', 'Aceptar', {
          duration: 1500,
        });
      });
  }

  getInformacionCongresos() {
    return this.informacion;
  }

  getInformationApp() {
    return this.information;
  }

  getInformacionCongreso(idCongreso: any) {
    return this.informacion[(idCongreso - 1)];
  }

  informacionRondaInversion(rondaInversion: string) {
    return this.afs
      .collection('rondas_inversion')
      .doc(rondaInversion)
      .valueChanges();
  }

  cambiaRondaInversion( idCongreso ) {
    this.afs
          .collection('aplicación')
          .doc(idCongreso.toString())
          .update({
            ronda_inversion_actual: this.getInformacionCongreso(idCongreso)['ronda_inversion_actual'] + 1
          })
          .then(valorGanadores => {
            this.snackBar.open('Se actualizaron correctamente los datos', 'Aceptar', {
              duration: 1500,
            });

          });
  }

  getAppInfo() {
    return this.appInfo;
  }
}
