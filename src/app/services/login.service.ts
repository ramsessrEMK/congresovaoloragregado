import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';



@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private username: string;
  private email_user: string;
  user: any;
  private userAuthenticated: boolean;
  constructor(
    public afAuth: AngularFireAuth,
  ) {
    if ( this.afAuth.auth.currentUser != null ) {
      this.userAuthenticated = true;
    } else {
      this.userAuthenticated = false;
    }
  }

  logIn(email, password) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }

}
