import { TestBed, inject } from '@angular/core/testing';

import { AdministrarUsuariosService } from './administrar-usuarios.service';

describe('AdministrarUsuariosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AdministrarUsuariosService]
    });
  });

  it('should be created', inject([AdministrarUsuariosService], (service: AdministrarUsuariosService) => {
    expect(service).toBeTruthy();
  }));
});
