import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { LogInComponent } from './components/log-in/log-in.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ScheduleComponent } from './components/schedule/schedule.component';
import { DesignThinkingComponent } from './components/design-thinking/design-thinking.component';
import { InvestmentsComponent } from './components/investments/investments.component';
import { AuthGuard } from './auth/auth.guard';
import {HomeLayoutComponent} from './layouts/home-layout/home-layout.component';
import {LoginLayoutComponent} from './layouts/login-layout/login-layout.component';
import {AdminLayoutComponent} from './layouts/admin-layout/admin-layout.component';
import {CoordinadoresLayoutComponent} from './layouts/coordinadores-layout/coordinadores-layout.component';
import {NavBarComponent} from './nav-bar/nav-bar.component';
import { from } from 'rxjs';
import { HomeAdminComponent } from './components/home-admin/home-admin.component';
import { HomeCoordinadoresComponent } from './components/home-coordinadores/home-coordinadores.component';
import { CrearOpcionesInversionComponent } from './components/crear-opciones-inversion/crear-opciones-inversion.component';
import { CrearUsuarioComponent } from './components/crear-usuario/crear-usuario.component';
import { LaboratoriosComponent } from './components/laboratorios/laboratorios.component';
import { AdminRondasInversionComponent } from './components/admin-rondas-inversion/admin-rondas-inversion.component';
import { AdministrarAgendaComponent } from './components/administrar-agenda/administrar-agenda.component';

const routes: Routes = [
  {
    path: '',                       // {1}
    component: HomeLayoutComponent,
    canActivate: [AuthGuard],       // {2}
    children: [
      {
        path: '',
        component: ProfileComponent,  // {3}
        pathMatch: 'full'
      },
      {
        path: 'schedule',
        component: ScheduleComponent,
        pathMatch: 'full'
      },
      {
        path: 'labs',
        component: LaboratoriosComponent,
        pathMatch: 'full'
      },
      {
        path: 'design_thinking/:id',
        component: DesignThinkingComponent,
      },
      {
        path: 'investment',
        component: InvestmentsComponent,
      },
    ]
  },
  {
    path: '',
    component: LoginLayoutComponent, // {4}
    children: [
      {
        path: 'login',
        component: LogInComponent   // {5}
      }
    ]
  },
  {
    path: 'admin',
    component: AdminLayoutComponent, // {4}
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: HomeAdminComponent,   // {5}
        pathMatch: 'full'
      },
      {
        path: 'incribirUsuarios',
        component: CrearUsuarioComponent,   // {5}
        pathMatch: 'full'
      },
      {
        path: 'rondaInversion/:id',
        component: AdminRondasInversionComponent,   // {5}
        pathMatch: 'full'
      },
      {
        path: 'calendario/:id',
        component: AdministrarAgendaComponent,   // {5}
        pathMatch: 'full'
      },
    ]
  },
  {
    path: 'coor',
    component: CoordinadoresLayoutComponent, // {4}
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: HomeCoordinadoresComponent,   // {5}
        pathMatch: 'full'
      },
      {
        path: 'inversiones',
        component: InvestmentsComponent,   // {5}
        pathMatch: 'full'
      },
      {
        path: 'nuevaOpcionInversion/:id',
        component: CrearOpcionesInversionComponent,   // {5}
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
